package kutlev.insuranceapp.manager;

import de.greenrobot.event.EventBus;

public final class EventManager {

    private static EventManager singleton;

    private EventBus eventBus;

    private EventManager() {
        eventBus = EventBus.getDefault();
    }

    public static synchronized EventManager getInstance() {
        if (singleton == null) {
            singleton = new EventManager();
        }

        return singleton;
    }

    /**
     * Registers the given subscriber to receive events.
     */
    public void register(Object subscriber) {
        eventBus.register(subscriber);
    }

    /**
     * Unregisters the given subscriber from all event classes.
     */
    public void unregister(Object subscriber) {
        eventBus.unregister(subscriber);
    }

    public boolean isRegistered(Object subscriber) {
        return eventBus.isRegistered(subscriber);
    }

    public void postEvent(Object event) {
        eventBus.post(event);
    }

}
