package kutlev.insuranceapp.util;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public final class ScreenUtil {

    private ScreenUtil() {
    }

    /**
     * Hides the keyboard if it is visible.
     *
     * @param activity the activity displaying the keyboard
     */
    public static void hideKeyboard(Activity activity) {
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null && focusedView instanceof EditText) {
            InputMethodManager input = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (input != null) {
                focusedView.clearFocus();
                input.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
            }
        }
    }

}



