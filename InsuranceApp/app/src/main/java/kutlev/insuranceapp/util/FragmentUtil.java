package kutlev.insuranceapp.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public final class FragmentUtil {

    private FragmentUtil() {
    }

    public static void addFragmentInView(FragmentManager fragmentManager, Fragment fragment,
                                         int containerViewId, boolean addToBackStack) {
        addFragmentInView(fragmentManager, fragment, containerViewId, addToBackStack, false);
    }

    public static void addFragmentInView(FragmentManager fragmentManager, Fragment fragment,
                                         int containerViewId, boolean addToBackStack, boolean replace) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (replace) {
            transaction.replace(containerViewId, fragment);
        } else {
            transaction.add(containerViewId, fragment);
        }

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

}
