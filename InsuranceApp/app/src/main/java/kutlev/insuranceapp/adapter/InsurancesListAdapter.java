package kutlev.insuranceapp.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import kutlev.insuranceapp.InsuranceApplication;
import kutlev.insuranceapp.R;

/**
 * Adapter for list of insurances.
 */
public class InsurancesListAdapter extends BaseAdapter {

    private static final String TAG = InsurancesListAdapter.class.getSimpleName();

    private static final int ELEMENTS_COUNT = 7;
    private float elevation;

    private LayoutInflater layoutInflater;

    public InsurancesListAdapter() {
        Log.d(TAG, "InsurancesListAdapter create");
        InsuranceApplication application = InsuranceApplication.get();
        layoutInflater = (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        elevation = application.getResources().getDimensionPixelSize(R.dimen.insurance_item_card_elevation);
    }

    @Override
    public int getCount() {
        return ELEMENTS_COUNT;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView;
        if (convertView == null) {
            itemView = layoutInflater.inflate(R.layout.insurance_list_item, parent, false);
        } else {
            itemView = convertView;
        }

        CardView cardView = (CardView) itemView.findViewById(R.id.insurance_card);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cardView.setElevation(elevation);
        } else {
            cardView.setCardElevation(elevation);
        }

        ImageView iconView = (ImageView) itemView.findViewById(R.id.insurance_icon);
        int iconId;
        if (position % 2 == 0) {
            iconId = R.drawable.insurance_house;
        } else if (position % 3 == 0) {
            iconId = R.drawable.insurance_car;
        } else {
            iconId = R.drawable.insurance_life;
        }
        iconView.setImageResource(iconId);

        TextView nameView = (TextView) itemView.findViewById(R.id.insurance_name);
        int nameId;
        if (position % 2 == 0) {
            nameId = R.string.houseInsurance;
        } else if (position % 3 == 0) {
            nameId = R.string.carInsurance;
        } else {
            nameId = R.string.lifeInsurance;
        }
        nameView.setText(nameId);

        return itemView;
    }

}