package kutlev.insuranceapp.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;

import kutlev.insuranceapp.R;
import kutlev.insuranceapp.activity.base.BaseActivity;
import kutlev.insuranceapp.enums.Screen;
import kutlev.insuranceapp.event.NavigationEvent;
import kutlev.insuranceapp.fragment.LoginFragment;
import kutlev.insuranceapp.fragment.PinVerificationFragment;
import kutlev.insuranceapp.fragment.SignUpFragment;
import kutlev.insuranceapp.manager.EventManager;
import kutlev.insuranceapp.util.FragmentUtil;

public class AuthenticationActivity extends BaseActivity {

    private static final String TAG = AuthenticationActivity.class.getSimpleName();

    protected EventManager eventManager;
    private int containerViewId;
    private EventProblemFix eventProblemFix;

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // Events set up
        eventManager = EventManager.getInstance();
        eventProblemFix = new EventProblemFix();
        eventManager.register(eventProblemFix);

        setContentView(R.layout.activity_authentication);
        containerViewId = R.id.container;
        actionBar = getSupportActionBar();

        if (savedInstanceState == null) {
            openLogin();
        }
    }

    @Override
    protected void onDestroy() {
        eventManager.unregister(eventProblemFix);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /**
     * Called when {@link kutlev.insuranceapp.event.NavigationEvent} is sent.
     *
     * @param event sent event
     */
    public void onEvent(NavigationEvent event) {
        Log.d(TAG, "onEvent - NavigationEvent");
        if (event.getScreen() == Screen.LOGIN) {
            openLogin();
        } else if (event.getScreen() == Screen.SIGN_UP) {
            openSignUp();
        } else if (event.getScreen() == Screen.PIN_VERIFICATION) {
            openPinVerification();
        }
    }

    private void openLogin() {
        actionBar.setTitle(R.string.login);
        LoginFragment fragment = LoginFragment.newInstance();
        FragmentUtil.addFragmentInView(getSupportFragmentManager(), fragment, containerViewId, false, true);
    }

    private void openSignUp() {
        actionBar.setTitle(R.string.signUp);
        SignUpFragment fragment = SignUpFragment.newInstance();
        FragmentUtil.addFragmentInView(getSupportFragmentManager(), fragment, containerViewId, false, true);
    }

    private void openPinVerification() {
        actionBar.setTitle(R.string.verifyPhone);
        PinVerificationFragment fragment = PinVerificationFragment.newInstance();
        FragmentUtil.addFragmentInView(getSupportFragmentManager(), fragment, containerViewId, false, true);
    }

    /**
     * This class is used to fix problem with EventBus library.
     * Remove after it is fixed.
     */
    class EventProblemFix {

        public EventProblemFix() {
        }

        @SuppressWarnings("UnusedDeclaration")
        public void onEvent(NavigationEvent event) {
            AuthenticationActivity.this.onEvent(event);
        }

    }

}
