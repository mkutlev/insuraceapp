package kutlev.insuranceapp.activity.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import kutlev.insuranceapp.InsuranceApplication;

/**
 * Parent activity for all activities in the application.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected InsuranceApplication application;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        application = InsuranceApplication.get();
    }

}
