package kutlev.insuranceapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import kutlev.insuranceapp.R;
import kutlev.insuranceapp.activity.MainActivity;
import kutlev.insuranceapp.enums.Screen;
import kutlev.insuranceapp.event.NavigationEvent;
import kutlev.insuranceapp.fragment.base.BaseFragment;
import kutlev.insuranceapp.manager.EventManager;
import kutlev.insuranceapp.util.ScreenUtil;

public class PinVerificationFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = PinVerificationFragment.class.getSimpleName();

    private EventManager eventManager;

    private EditText codeField;
    private View proceedButton;
    private View loginButton;

    public static PinVerificationFragment newInstance() {
        return new PinVerificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        eventManager = EventManager.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pin_verification, container, false);

        codeField = (EditText) rootView.findViewById(R.id.pin_verification_code);
        codeField.setImeOptions(EditorInfo.IME_ACTION_DONE);
        codeField.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    proceedButton.performClick();
                    return true;
                }

                return false;
            }

        });

        proceedButton = rootView.findViewById(R.id.pin_verification_proceed);
        proceedButton.setOnClickListener(this);
        loginButton = rootView.findViewById(R.id.pin_verification_login);
        loginButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == proceedButton.getId()) {
            verify();
        } else if (v.getId() == loginButton.getId()) {
            openLogin();
        }
    }

    private void verify() {
        ScreenUtil.hideKeyboard(getActivity());
        openMainActivity();
    }

    private void openMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void openLogin() {
        ScreenUtil.hideKeyboard(getActivity());
        NavigationEvent event = new NavigationEvent(Screen.LOGIN);
        eventManager.postEvent(event);
    }

}
