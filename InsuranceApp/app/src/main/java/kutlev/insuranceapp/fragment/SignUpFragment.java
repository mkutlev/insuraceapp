package kutlev.insuranceapp.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import kutlev.insuranceapp.R;
import kutlev.insuranceapp.enums.Screen;
import kutlev.insuranceapp.event.NavigationEvent;
import kutlev.insuranceapp.fragment.base.BaseFragment;
import kutlev.insuranceapp.manager.EventManager;
import kutlev.insuranceapp.util.ScreenUtil;

public class SignUpFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = SignUpFragment.class.getSimpleName();

    private EventManager eventManager;

    private View rootView;
    private EditText emailField;
    private EditText passwordField;
    private EditText repeatPasswordField;
    private EditText phoneField;

    private CheckBox acceptTerms;
    private CheckBox acceptPrivacy;
    private CheckBox receiveNewsletters;

    private View signUpButton;
    private View haveAccountButton;
    private View termsButton;

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        eventManager = EventManager.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);

        setUpTextFields();
        setUpCheckBoxes();
        setUpButtons();

        return rootView;
    }

    private void setUpTextFields() {
        emailField = (EditText) rootView.findViewById(R.id.sign_up_email);
        passwordField = (EditText) rootView.findViewById(R.id.sign_up_password);
        repeatPasswordField = (EditText) rootView.findViewById(R.id.sign_up_repeat_password);
        phoneField = (EditText) rootView.findViewById(R.id.sign_up_phone);
    }

    private void setUpCheckBoxes() {
        acceptTerms = (CheckBox) rootView.findViewById(R.id.sign_up_accept_terms);
        acceptPrivacy = (CheckBox) rootView.findViewById(R.id.sign_up_accept_privacy);
        receiveNewsletters = (CheckBox) rootView.findViewById(R.id.sign_up_receive_newsletters);
    }

    private void setUpButtons() {
        signUpButton = rootView.findViewById(R.id.sign_up_sign_up);
        signUpButton.setOnClickListener(this);
        haveAccountButton = rootView.findViewById(R.id.sign_up_have_account);
        haveAccountButton.setOnClickListener(this);
        termsButton = rootView.findViewById(R.id.sign_up_terms);
        termsButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == signUpButton.getId()) {
            signUp();
        } else if (v.getId() == haveAccountButton.getId()) {
            openLogin();
        } else if (v.getId() == termsButton.getId()) {
            openTerms();
        }
    }

    private void signUp() {
        ScreenUtil.hideKeyboard(getActivity());

        // TODO Validate fields

        openPinVerification();
    }

    private void openLogin() {
        ScreenUtil.hideKeyboard(getActivity());
        NavigationEvent event = new NavigationEvent(Screen.LOGIN);
        eventManager.postEvent(event);
    }

    private void openTerms() {
        ScreenUtil.hideKeyboard(getActivity());
        Toast.makeText(getActivity(), "Terms and Conditions", Toast.LENGTH_SHORT).show();
    }

    private void openPinVerification() {
        ScreenUtil.hideKeyboard(getActivity());
        NavigationEvent event = new NavigationEvent(Screen.PIN_VERIFICATION);
        eventManager.postEvent(event);
    }

}
