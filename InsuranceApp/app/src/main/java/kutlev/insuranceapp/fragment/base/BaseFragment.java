package kutlev.insuranceapp.fragment.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import kutlev.insuranceapp.InsuranceApplication;

/**
 * Parent fragment for all fragments in the application.
 */
public abstract class BaseFragment extends Fragment {

    protected InsuranceApplication application;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = InsuranceApplication.get();
    }

}
