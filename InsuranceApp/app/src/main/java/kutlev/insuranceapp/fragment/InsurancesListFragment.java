package kutlev.insuranceapp.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import kutlev.insuranceapp.R;
import kutlev.insuranceapp.activity.MainActivity;
import kutlev.insuranceapp.adapter.InsurancesListAdapter;

/**
 * A fragment representing a list of all alarms.
 */
public class InsurancesListFragment extends ListFragment {

    private static final String TAG = InsurancesListFragment.class.getSimpleName();

    private static final int SECTION_NUMBER = 1;

    private InsurancesListAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public InsurancesListFragment() {
    }

    public static InsurancesListFragment newInstance() {
        Log.d(TAG, "newInstance");
        return new InsurancesListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new InsurancesListAdapter();
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_insurances_list, container, false);
        ListView listView = (ListView) rootView.findViewById(android.R.id.list);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(SECTION_NUMBER);
    }

}
