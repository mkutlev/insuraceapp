package kutlev.insuranceapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import kutlev.insuranceapp.R;
import kutlev.insuranceapp.activity.MainActivity;
import kutlev.insuranceapp.enums.Screen;
import kutlev.insuranceapp.event.NavigationEvent;
import kutlev.insuranceapp.fragment.base.BaseFragment;
import kutlev.insuranceapp.manager.EventManager;
import kutlev.insuranceapp.util.ScreenUtil;

public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = LoginFragment.class.getSimpleName();

    private EventManager eventManager;

    private EditText emailField;
    private EditText passwordField;
    private View loginButton;
    private View signUpButton;
    private View forgotPasswordButton;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        eventManager = EventManager.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        emailField = (EditText) rootView.findViewById(R.id.login_email);
        passwordField = (EditText) rootView.findViewById(R.id.login_password);
        passwordField.setImeOptions(EditorInfo.IME_ACTION_DONE);
        passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginButton.performClick();
                    return true;
                }

                return false;
            }

        });

        loginButton = rootView.findViewById(R.id.login_login);
        loginButton.setOnClickListener(this);
        signUpButton = rootView.findViewById(R.id.login_sign_up);
        signUpButton.setOnClickListener(this);
        forgotPasswordButton = rootView.findViewById(R.id.login_forgot_password);
        forgotPasswordButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == loginButton.getId()) {
            login();
        } else if (v.getId() == signUpButton.getId()) {
            openSignUp();
        } else if (v.getId() == forgotPasswordButton.getId()) {
            openForgotPassword();
        }
    }

    private void login() {
        ScreenUtil.hideKeyboard(getActivity());

        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void openSignUp() {
        ScreenUtil.hideKeyboard(getActivity());
        NavigationEvent event = new NavigationEvent(Screen.SIGN_UP);
        eventManager.postEvent(event);
    }

    private void openForgotPassword() {
        ScreenUtil.hideKeyboard(getActivity());
        Toast.makeText(getActivity(), "Forgot Password", Toast.LENGTH_SHORT).show();
    }

}
