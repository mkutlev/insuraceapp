package kutlev.insuranceapp.event;

import android.os.Bundle;

import kutlev.insuranceapp.enums.Screen;

public class NavigationEvent {

    private Screen screen;
    private Bundle arguments;

    public NavigationEvent(Screen screen, Bundle arguments) {
        this.screen = screen;
        this.arguments = arguments;
    }

    public NavigationEvent(Screen screen) {
        this(screen, null);
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public Bundle getArguments() {
        return arguments;
    }

    public void setArguments(Bundle arguments) {
        this.arguments = arguments;
    }

}
