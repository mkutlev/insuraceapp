package kutlev.insuranceapp;

import android.app.Application;

public class InsuranceApplication extends Application {

    private static InsuranceApplication application;

    public static InsuranceApplication get() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }

}
